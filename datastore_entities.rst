3. Cloud Datastore エンティティ
===============================

3-1. イベントテンプレート(EventTemplate)
---------------------------------------

========= ======== ==================== ================
物理名    型       値                   インデックス登録
========= ======== ==================== ================
EventName String   イベントの名前       true
EventDesc String   イベントの概要       false
EventDate DateTime イベントの開始予定日 false
========= ======== ==================== ================
