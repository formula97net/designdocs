.. ParticipantManagerDoc documentation master file, created by
   sphinx-quickstart on Sun Jan 29 21:35:47 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

参加者管理システム
==================

Contents:

.. toctree::
   :maxdepth: 2

   overview
   screen_transition
   datastore_entities


索引/目次
=========

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

