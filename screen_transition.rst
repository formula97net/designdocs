2. 画面遷移
===========

.. blockdiag::
   :desctable:

   blockdiag admin {
       A -> B -> C;
            B -> D;

       A [description = "ログイン(共通)"];
       B [description = "管理メニュー"];
       C [description = "イベント管理"];
       D [description = "通信文テンプレート"];
   }

   